version: 2

models:
    - name: zuora_accounts_receivables
      description: Zuora accounts receivable
      columns:
          - name: invoice_id
            tests:
                - not_null
                - unique
    - name: zuora_base_mrr
      description: '{{ doc("zuora_base_mrr") }}'
      columns:
          - name: mrr
            description: Monthly recurring revenue
            tests:
                - not_null
          - name: lineage
            description: "All the subscriptions subsquent to the primary subscription, separated by commas."
    - name: zuora_base_mrr_amortized
      description: '{{ doc("zuora_base_mrr_amortized") }}'
      columns:
          - name: account_number
            tests:
                - not_null
          - name: subscription_name
            tests:
                - not_null
          - name: subscription_name_slugify
            tests:
                - not_null
          - name: oldest_subscription_in_cohort
            tests:
                - not_null
          - name: lineage
            description: "All the subscriptions subsquent to the primary subscription, separated by commas."
            tests:
                - not_null
          - name: rate_plan_name
            tests:
                - not_null
          - name: rate_plan_charge_name
            tests:
                - not_null
          - name: mrr
            description: Monthly recurring revenue
            tests:
                - not_null
          - name: mrr_month
            description: The month the MRR is to be applied to
            tests:
                - not_null
          - name: cohort_month
            tests:
                - not_null
          - name: cohort_quarter
            tests:
                - not_null
          - name: unit_of_measure
            tests:
                - not_null
          - name: quantity
            tests:
                - not_null
          - name: product_category
            description: Product category is based on the rate plan name.
            tests:
              - accepted_values:
                      values: ['GitHost', 'Other', 'Gold', 'Basic', 'Bronze', 'Premium', 'Starter', 'Ultimate', 'Support', 'Plus', 'Silver', 'Standard', 'Trueup']
    - name: zuora_base_invoice_details
      description: '{{ doc("zuora_base_invoice_details") }}'
    - name: zuora_base_ci_minutes
      description: '{{ doc("zuora_base_ci_minutes") }}'
      columns:
          -   name: charge_name
              description: The CI charge
              tests:
                  - not_null
                  -   accepted_values:
                          values: ['1,000 CI Minutes', '1,000 CI Minutes Credit']
    - name: zuora_base_trueups
      description: '{{ doc("zuora_base_trueups") }}'
      columns:
          - name: charge_name
            description: The type of trueup
            tests:
                - not_null
                - accepted_values:
                        values: ['Trueup','Trueup Credit']
          - name: lineage
            description: "All the subscriptions subsquent to the primary subscription, separated by commas."
    - name: zuora_current_arr
      description: Zuora current ARR
      columns:
          - name: current_arr
            tests:
                - not_null
    - name: zuora_mrr_totals
      description: '{{ doc("zuora_mrr_totals") }}'
      columns:
          - name: primary_key
            tests:
                - not_null
                - unique
          - name: account_number
            description: '{{ doc("zuora_mrr_totals_col_account_number") }}'
            tests:
                - not_null
          - name: subscription_name_slugify
            description: '{{ doc("zuora_mrr_totals_col_subscription_name_slugify") }}'
            tests:
                - not_null
          - name: subscription_name
            description: '{{ doc("zuora_mrr_totals_col_subscription_name") }}'
            tests:
                - not_null
          - name: oldest_subscription_in_cohort
            description: '{{ doc("zuora_mrr_totals_col_subscription_slug_for_counting") }}'
            tests:
                - not_null
          - name: lineage
            tests:
                - not_null
          - name: mrr_month
            description: '{{ doc("zuora_mrr_totals_col_mrr_month") }}'
            tests:
                - not_null
            tests:
                - not_null
          - name: zuora_subscription_cohort_month
            description: '{{ doc("zuora_mrr_totals_col_cohort_month") }}'
            tests:
                - not_null
          - name: zuora_subscription_cohort_quarter
            description: '{{ doc("zuora_mrr_totals_col_cohort_quarter") }}'
            tests:
                - not_null
          - name: product_category
          - name: unit_of_measure
          - name: mrr
            description: '{{ doc("zuora_mrr_totals_col_mrr") }}'
            tests:
                - not_null
          - name: months_since_zuora_subscription_cohort_start
            description: '{{ doc("zuora_mrr_totals_col_months_since_cohort_start") }}'
            tests:
                - not_null
          - name: quarters_since_zuora_subscription_cohort_start
            description: '{{ doc("zuora_mrr_totals_col_quarters_since_cohort_start") }}'
            tests:
                - not_null
    - name: zuora_scheduled_renewals
      description: Zuora Scheduled Renewals
      columns:
          - name: account_name 
            tests:
                - not_null
          - name: account_number
            tests:
                - not_null
          - name: rate_plan_charge_name
            tests:
                - not_null
          - name: rate_plan_charge_number
            tests:
                - not_null
          - name: currency
            tests:
                - not_null
          - name: effective_start_date
            tests:
                - not_null
          - name: effective_end_date
            tests:
                - not_null
          - name: subscription_start_date
            tests:
                - not_null
          - name: mrr
            tests:
                - not_null
          - name: arr
            tests:
                - not_null                
    - name: zuora_subscription_intermediate
      description: '{{ doc("zuora_subscription_intermediate") }}'
      columns:
          - name: sub_row
            tests:
                - not_null
                - accepted_values:
                        values: ['1']
          - name: subscription_id
            tests:
                - not_null
                - unique
          - name: subscription_status
            tests:
                - not_null
                - accepted_values:
                        values: ['Active','Cancelled']
    - name: zuora_subscription_lineage
      description:  '{{doc("zuora_subscription_lineage")}}'
      columns: 
          - name: subscription_name_slugify
            description: '{{ doc("zuora_mrr_totals_col_subscription_name_slugify") }}'
            tests:
              - not_null
              - unique
          - name: lineage
            description: "All the subscriptions subsquent to the primary subscription, separated by commas."
    - name: zuora_subscription_parentage_finish
      description: '{{ doc("zuora_subscription_parentage_finish") }}'
      columns:
          - name: ultimate_parent_sub
            tests:
              - not_null
          - name: child_sub
            tests:
              - not_null
              - unique
          - name: cohort_month
            tests:
              - not_null
          - name: cohort_quarter
            tests:
              - not_null
          - name: cohort_year
            tests:
              - not_null
    - name: zuora_subscription_parentage_start
      description: '{{doc("zuora_subscription_parentage_start")}}'
    - name: zuora_subscription_xf
      description: This models takes the `zuora_subscription_intermediate` model and joins it to the `zuora_subscription_lineage` model. The end result is every subscription is linked to its lineage (or null).
      columns:
          - name: sub_row
            tests:
                - not_null
                - accepted_values:
                        values: ['1']
          - name: subscription_id
            tests:
                - not_null
                - unique
          - name: subscription_status
            tests:
                - not_null
                - accepted_values:
                        values: ['Active','Cancelled']
          - name: lineage
            description: "All the subscriptions subsquent to the primary subscription, separated by commas."
          - name: cohort_month
            tests:
                - not_null
          - name: cohort_quarter
            tests:
                - not_null
          - name: cohort_year
            tests:
                - not_null

